#Sample Leads CRUD App for MSC

##Ryan Pfahl

##View Running App
Visit https://msc-lead-app-rmmpfahl.c9users.io/ to see the live app.

Create new user OR to access admin functions use the following credentials:
admin@admin.com
123456

###Original Spec
1. Build CRUD for a lead (first name, last name, phone, email)

###Bonus Items
1. Validations requiring all fields
2. Validations ensuring unique email
3. Implement the Devise Gem for authentication
4. Build seeds file for leads
5. Ensure Logged in users can access CRUD actions for leads (otherwise, redirect)

###Other Features
I implemented a few additional features:

1. Added email validator to requrie valid email addresses (for lead model)
2. Included support for error messages on lead submission
3. Added additional db column to track leads that have been contacted, which can be set true on the list or update views
4. Added a simple search feature using scopes to search for name, last name, or email; also used this to allow 'contacted' leads to be filtered
5. Added pagination for the leads list using the Kaminari Gem
6. Added admin role to allow some CRUD actions on users (index, edit, and destroy)
7. Used Bootstrap to add some basic page styling

