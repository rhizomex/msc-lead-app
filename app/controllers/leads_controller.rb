class LeadsController < ApplicationController
    before_action :authenticate_user!
    def index
        @leads = Lead.page(params[:page])
        
        filtered(params).each do |k, v|
           @leads = @leads.public_send(k, v) if v.present?
       end
    end
    
    def show
        @lead = Lead.find(params[:id])
    end
    
    def new
        @lead = Lead.new
    end
    
    def edit
        @lead = Lead.find(params[:id])
    end
    
    def create
        @lead = Lead.new(lead_params)
        if @lead.save
            redirect_to @lead
        else
            render 'new'
        end
    end
    
    def update
        @lead = Lead.find(params[:id])
        if @lead.update(lead_params)
            redirect_to @lead
        else
            render 'edit'
        end
    end
    
    def destroy
        @lead = Lead.find(params[:id])
        @lead.destroy
        redirect_to leads_path
    end
    
    def contacted
        @lead = Lead.find(params[:id])
        
        @lead.update(:contacted => true)
            redirect_to leads_path
    end
    
    def search
      search = params[:search]
      search_string =  search.to_str.gsub(' ', '&')
      redirect_to "/leads?search=#{search_string}"
    end
    
    private
        def lead_params
            params.require(:lead).permit(:first_name, :last_name, :phone, :email, :contacted)
        end
        
        def filtered(params)
          params.slice(:contacted, :uncontacted, :first_name_contains, :last_name_contains, :email_contains, :search)
        end
end
