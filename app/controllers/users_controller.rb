class UsersController < ApplicationController
    before_action :authenticate_user!
    before_filter :ensure_admin!
    
    def index
        @users = User.page(params[:page])
    end
    
   def edit
        @user = User.find(params[:id])
    end
    
    def update
        @user = User.find(params[:id])
        if @user.update(user_params)
            redirect_to users_path
        else
            render 'edit'
        end
    end
    
    def destroy
        @user = User.find(params[:id])
        @user.destroy
        redirect_to users_path
    end
    
    private
        def user_params
            params.require(:user).permit(:name, :email, :admin)
        end
        
        def ensure_admin!
            unless current_user.admin?
                redirect_to root_path
                return false
            end
        end
end
