class Lead < ApplicationRecord
    validates :first_name, :last_name, :phone, :email, presence: true
    validates :email, uniqueness: true, email: true
    
    scope :contacted, -> (contacted)  { where(contacted: true ) }
    scope :uncontacted, -> (uncontacted) { where(contacted: false ) }
    scope :first_name_contains, -> (first_name) { where("first_name like ?", "#{first_name}%")}
    scope :last_name_contains, -> (last_name) { where("last_name like ?", "#{last_name}%")}
    scope :email_contains, -> (email) { where("email like ?", "#{email}%")}
    
    scope :search, -> (search) { first_name_contains(search).or(last_name_contains(search)).or(email_contains(search)) }
   
end
