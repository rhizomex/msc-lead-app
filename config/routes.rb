Rails.application.routes.draw do
  
  devise_for :users

  resources :leads
  
  patch '/leads/:id/edit(.:format)' => 'leads#contacted'
  post '/search' => 'leads#search'
  
  resources :users, only: [:index, :edit, :update, :destroy]

  root to: redirect('/leads') 
end
