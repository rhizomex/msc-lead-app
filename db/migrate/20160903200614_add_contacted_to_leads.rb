class AddContactedToLeads < ActiveRecord::Migration[5.0]
  def change
    add_column :leads, :contacted, :boolean
  end
end
