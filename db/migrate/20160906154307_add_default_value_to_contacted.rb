class AddDefaultValueToContacted < ActiveRecord::Migration[5.0]
  def change
    change_column :leads, :contacted, :boolean, :default => false
  end
end
