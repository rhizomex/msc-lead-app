# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Lead.create!([{
    first_name: "Ryan",
    last_name: "Pfahl",
    phone: "253-123-4567",
    email: "1email@email.com"
},
{
    first_name: "Kevin",
    last_name: "Chugh",
    phone: "716-123-4568",
    email: "2email@email.com"
},
{
    first_name: "Dorian",
    last_name: "Grey",
    phone: "716-123-4566",
    email: "3email@gmail.com"
},
{
    first_name: "Gene",
    last_name: "Wilder",
    phone: "716-123-4565",
    email: "rip@theafterlife.com"
},
{
    first_name: "Gilles",
    last_name: "Deleuze",
    phone: "716-123-3567",
    email: "4email@gmail.com"
},
{
    first_name: "Immanuel",
    last_name: "Kant",
    phone: "716-123-4667",
    email: "5email@email.com"
},
{
    first_name: "Gottfried",
    last_name: "Leibniz",
    phone: "716-123-4577",
    email: "6email@yahoo.com"
},
{
    first_name: "Baruch",
    last_name: "Spinoza",
    phone: "716-123-4587",
    email: "1email@aol.com"
},
{
    first_name: "Friedrich",
    last_name: "Nietzsche",
    phone: "716-123-1881",
    email: "zarathustra@thegreatnoon.com"
},
{
    first_name: "Georg",
    last_name: "Hegel",
    phone: "716-123-1567",
    email: "absolutespirit@email.com"
},
{
    first_name: "Betrand",
    last_name: "Russel",
    phone: "716-120-4567",
    email: "math@email.com"
},
{
    first_name: "Dr.",
    last_name: "Jekyll",
    phone: "555-123-4567",
    email: "drjekyllforpresident@gmail.com"
},
{
    first_name: "David",
    last_name: "Hume",
    phone: "123-123-4567",
    email: "25email@email.com"
},
{
    first_name: "Fancis",
    last_name: "Bacon",
    phone: "717-123-4567",
    email: "26email@email.com"
},
{
    first_name: "Issac",
    last_name: "Newton",
    phone: "718-123-4567",
    email: "27email@email.com"
},
{
    first_name: "Rene",
    last_name: "Descartes",
    phone: "719-123-4567",
    email: "28email@email.com"
},
{
    first_name: "John",
    last_name: "Lock",
    phone: "710-123-4567",
    email: "29email@email.com"
},
{
    first_name: "Thomas",
    last_name: "Hobbes",
    phone: "711-123-4567",
    email: "30email@email.com"
},
{
    first_name: "Michel",
    last_name: "Foucault",
    phone: "712-123-4567",
    email: "31email@email.com"
},
{
    first_name: "Jacques",
    last_name: "Derrida",
    phone: "713-123-4567",
    email: "32email@email.com"
},
{
    first_name: "Alan",
    last_name: "Turing",
    phone: "714-123-4567",
    email: "33email@email.com"
},
{
    first_name: "Jon",
    last_name: "Stewart",
    phone: "715-123-4567",
    email: "34email@email.com"
},
{
    first_name: "Jon",
    last_name: "Oliver",
    phone: "700-123-4567",
    email: "35email@email.com"
},
{
    first_name: "Samantha",
    last_name: "Bee",
    phone: "701-123-4567",
    email: "36email@email.com"
},
{
    first_name: "Katharine",
    last_name: " Hepburn",
    phone: "702-123-4567",
    email: "37email@email.com"
},
{
    first_name: "Myrna",
    last_name: "Loy",
    phone: "703-123-4567",
    email: "38email@email.com"
}])